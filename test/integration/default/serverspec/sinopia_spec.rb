require 'serverspec'

set :backend, :exec

# Sinopia service
describe service('sinopia') do
	it { should be_enabled }
	it { should be_running }
end

describe port(4873) do
	it { should be_listening }
end

#!/usr/bin/env bats

@test "sinopia binary is found in PATH" {
  run which sinopia
  [ "$status" -eq 0 ]
}

@test "sinopia configuration exists" {
  cat /etc/sinopia/config.yaml
}

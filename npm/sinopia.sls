{%- from "npm/map.jinja" import sinopia with context %}

sinopia:
  npm.installed:
    - name: {{ sinopia.pkg_name }}
  group:
    - present
  user: 
    - present
    - groups:
      - sinopia 
    - require:
      - group: sinopia

sinopia_conf_dir:
  file.directory:
    - user: sinopia
    - name: {{ salt['pillar.get']('sinopia:sinopia_conf_dir', '') }}
    - group: sinopia

sinopia_storage_dir:
  file.directory:
    - user: sinopia
    - name: {{ salt['pillar.get']('sinopia:sinopia_storage_dir', '') }}
    - group: sinopia

sinopia_logs_dir:
  file.directory:
    - user: sinopia
    - name: {{ salt['pillar.get']('sinopia:sinopia_logs_dir', '') }}
    - group: sinopia

/etc/sinopia/config.yaml:
  file:
    - managed
    - source:
      - salt://npm/files/default/etc/sinopia/config.yaml.jinja
      - salt://npm/files/default/etc/sinopia/config.yaml.jinja
    - template: jinja

/etc/init.d/sinopia:
  file:
    - managed
    - source:
      - salt://npm/files/default/etc/init.d/sinopia.jinja
      - salt://npm/files/default/etc/init.d/sinopia.jinja
    - template: jinja
    - mode: 755

{% if grains['os'] == 'Debian' %}
salt:
  module.run:
  - name: service.systemctl_reload 
{% endif %}

sinopia_service:
  service:
    - name: {{ sinopia.service }}
    - reload: True
    - enable: True
    - running
